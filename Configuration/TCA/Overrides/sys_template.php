<?php
/**
 * This file is part of the "fancybox" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'fancybox',
    'Configuration/TypoScript',
    'fancyBox'
);
